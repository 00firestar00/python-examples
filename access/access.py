#!/usr/bin/env python

import cgi
import cgitb
import math
import re

print "Content-type: text/html\n\n"
print "<h1>Wow! Much Access! Such Python!</h1>"
# Location of access_log on my CentOS server
f = open('/var/log/httpd/dev-accesslog','r')
d = {}
for line in f:
    regex = '([(\d\.)]+) (-|\w+) (-|\w+) \[(.*?)\] "(.*?)" (-|\d+) (-|\d+)'
    reg_match = re.match(regex, line)
    if reg_match.group(1) in d:
        if (reg_match.group(7) != '-'):
            d[reg_match.group(1)] += int(float(reg_match.group(7)))
    else:
        if (reg_match.group(7) != '-'):
            d[reg_match.group(1)] = int(float(reg_match.group(7)))
    
for k in d.keys():
    print "<li>Host: " + k + " recieved",d[k],"bytes</li>"   

f.close()
