#!/usr/bin/env python

import xml.etree.ElementTree as ET

tree = ET.parse('gamedb.txt')
root = tree.getroot()

def print_title(w,b,e,d,m,o,r,f):
    print '-' * 80
    print "%s vs. %s at %s on %s\nThe opening played is (%s) %s"%(w,b,e,d,o,m)
    if (r == "0-1"):
        print "Black wins this game"
    else:
        print "White wins this game"
    print "Here is the position at move " + f + "\n"

def format_row(row, row_num):
    i = 0
    row_num = row_num
    for c in row:
        if c.isdigit():
            i += int(c)
        else:
            chess_board[row_num][i] = c
            i += 1

def print_chess_board(fen):
    print "   a b c d e f g h"
    i = 7
    for line in fen.split('/'):
        row = format_row(line, i)
        print "%d "%(i+1),
        print " ".join(chess_board[i])
        i -= 1

def is_valid_move(knight,x,y):
    legal_moves = [ [-1,-2], [-2,-1], [-2,1], [-1,2], [1,2], [2,1], [2,-1], [1,-2] ]
    for moves in legal_moves:
        row = y + moves[0]
        column = x + moves[1]
        if (row < 8) and (row >= 0) and (column < 8) and (column >= 0):
            if knight.islower():
                if not chess_board[row][column].islower():
                    print "N%s%d"%(chr(column+97),row+1),
            else:
                if not chess_board[row][column].isupper():
                    print "N%s%d"%(chr(column+97),row+1),
    print ""

def print_legal_moves():
    print "\nHere are the legal knight moves:"
    row = 7
    for i in range(0,8):
        column = 0
        for piece in chess_board[row]:
            if (piece.lower() == "n"):                
                print "N%s%d:"%(chr(column+97),row+1),
                is_valid_move(piece,column,row)
            column += 1
        row -= 1

for game in root:
    chess_board = [["." for x in xrange(8)] for x in xrange(8)] 
    print_title(game.find('white').text, game.find('black').text, 
        game.find('event').text, game.find('date').text, 
        game.find('opening').text, game.find('opening').get('book'), 
        game.find('result').text, game.find('fen').get('move'))
    print_chess_board(game.find('fen').text)
    print_legal_moves()
