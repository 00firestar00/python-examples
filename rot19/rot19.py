#!/usr/bin/env python

import cgi
import cgitb
import sys
import string

print "Content-type: text/html\n\n"
print "<h1>Your decrypted message is:</h1>"

args = sys.stdin.read()
arg_list = args.split('&')

key, value = arg_list[0].split('=')
rot19 = string.maketrans( 
    "TUVWXYZABCDEFGHIJKLMNOPQRStuvwxyzabcdefghijklmnopqrs", 
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
rotation = string.translate(value, rot19)
print "<h2>" + rotation.replace('+',' ') + "</h2>"
