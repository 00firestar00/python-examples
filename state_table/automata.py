#!/usr/bin/python

import sys

# state       a       b
# E           E       B
# B           BA      B
# BA          E       BAB
# BAB         BA      BABB
# BABB        BABBA   B
# BABBA    

# Global variable "states" 
E = 0
B = 1
BA = 2
BAB = 3
BABB = 4
BABBA = 5

def create_table():
    # Zero index = a, First = b
    return [ [E,B],[BA, B],[E,BAB],[BA,BABB],[BABBA,B] ]

# Recursively check the table.
def check_table(string, state):
    global index
    if state == BABBA:
        return True
    if index == len(string):
        return False
    if string[index] == 'a':
        index += 1
        return check_table(string, table[state][0])
    if string[index] == 'b':
        index += 1
        return check_table(string, table[state][1])

length = len(sys.argv)
table = create_table()
table_length = len(table)
index = 0

# Allows for string of length 0
if length == 1:
    sys.exit("no")

# Too many inputs
if length > 2:
    sys.exit("Enter a language consisting of a and/or b (no spaces).")

if length == 2:
    # Convert string to char list
    input_string = list(sys.argv[1])
    # input must larger than the substring
    if len(input_string) < table_length:
        sys.exit("no")

    if check_table(input_string, 0):
        print "yes"
    else:
        print "no"