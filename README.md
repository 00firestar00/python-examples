Python Projects
=====================
This repo is to demonstrate some of my knowledge of python from assignments at school.

----------

##Access##
This script parses the access log for my CentOS server and displays it.

##Uptime##
Shows the uptime for my server.

##Rot 19##
Performs a rot19 encryption on an input string.

##Chess XML Parser##
Parses an XML and displays the data for chess tournaments in a user friendly fashion.

##State Table##
Checks if a certain string is allowed in the language based on a state table.