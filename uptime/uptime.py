#!/usr/bin/env python

import cgi
import cgitb
import math
import ConfigParser

def print_time(time):
    days = (time / 60 / 60 / 24)
    hours = (time / 60 / 60) - (days * 24)
    minutes = (time / 60) - (hours * 60) - (days * 24 * 60)
    seconds = (time % 60)
    print "<h3>",days,"days",hours,"hours",minutes,"minutes",seconds,"seconds</h3>"

print "Content-type: text/html\n\n"
print "<h1>Wow! Much Uptime! Such Python!</h1>"
f = open('/proc/uptime','r')
uptime_file = f.read()
f.close()
time_list = uptime_file.split(' ')
uptime = int(float(time_list[0]))
print "<h2>Much time. No accident:</h2>"
print_time(uptime)

f = open('record','r')
record = f.read()
f.close()
record_int = 0
if record:
    record_int = int(float(record))

if (record_int > uptime):
    print "<h2>Such record. No accident:</h2>"
    print_time(record_int)
else:
    f = open('record','w')
    print "<h2>This time. Such record. No accident:</h2>"
    print_time(uptime)
    f.write(str(uptime))
    f.close()
